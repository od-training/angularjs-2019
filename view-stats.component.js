import angular from "angular";

angular.module("app").component("viewStats", {
  template: "<pre>{{ $ctrl.views | json }}</pre>",
  bindings: {
    views: "<"
  }
});
