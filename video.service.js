import angular from "angular";

class VideoService {
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
    // this.getAllCompanyDetails().then(details => console.log(details));
  }

  getVideos() {
    // "../demo-data/videos.json"
    return this.$http.get("/api/videos").then(response => response.data);
  }

  addVideo(video) {
    return this.$http
      .post("/api/videos", video)
      .then(response => response.data);
  }

  getPreresolvedPromise() {
    return this.$q.resolve(4);
  }

  getVideo(id) {
    if (id === "85834") {
      return this.$q.resolve({});
    } else {
      return this.$http.get("/api/videos/" + id);
    }
  }

  getAllCompanyDetails() {
    return this.$http
      .get("/api/companies")
      .then(response => response.data.map(company => company.id))
      .then(companyIds =>
        this.$q.all(
          companyIds.map(id =>
            this.$http
              .get(`/api/companies/${id}`)
              .then(response => response.data)
          )
        )
      );
  }
}

angular.module("app").service("videoService", VideoService);
