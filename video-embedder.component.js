import angular from "angular";

angular.module("app").component("videoEmbedder", {
  template:
    "<a ng-href='https://youtu.be/{{$ctrl.selectedVideo}}'>{{ $ctrl.selectedVideo }}</a>",
  bindings: {
    selectedVideo: "<"
  }
});
