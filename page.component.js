import angular from "angular";

class PageController {
  constructor(state) {
    this.state = state;
  }

  addVideo() {
    console.log("adding");
    this.state.addVideo({
      title: "Generated Video",
      author: "A Robot",
      viewDetails: []
    });
  }
}

angular.module("app").component("pageComponent", {
  templateUrl: "page.component.html",
  controller: PageController
});
