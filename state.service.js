import angular from "angular";

class StateService {
  constructor(videoService) {
    this.videoService = videoService;
    this.videoList = [];
    this.filter = {
      region: "All"
    };
    this.selectedVideo = undefined;
    this.filteredViews = [];
    videoService.getVideos().then(videos => (this.videoList = videos));
  }
  setSelectedVideo(video) {
    this.selectedVideo = video;
    this.calculateFilter();
  }
  addVideo(video) {
    return this.videoService.addVideo(video).then(video => {
      this.videoList.push(video);
      return video;
    });
  }
  setFilter(filter) {
    this.filter = filter;
    this.calculateFilter();
  }

  calculateFilter() {
    if (this.selectedVideo) {
      this.filteredViews = this.selectedVideo.viewDetails.filter(
        view =>
          this.filter.region === "All" || view.region === this.filter.region
      );
    } else {
      this.filteredViews = [];
    }
  }
}

angular.module("app").service("state", StateService);
