import angular from "angular";

class VideoListController {
  constructor() {
    this.query = {};
  }
  selectVideo(video) {
    this.selectionMade({
      video
    });
  }
}

angular.module("app").component("videoList", {
  templateUrl: `video-list.component.html`,
  controller: VideoListController,
  bindings: {
    videoList: "<",
    selectedVideo: "<",
    selectionMade: "&"
  }
});
