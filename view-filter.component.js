import angular from "angular";

class ViewFilterController {
  constructor(state) {
    this.state = state;
    this.filter = state.filter;
  }
}

angular.module("app").component("viewFilter", {
  templateUrl: "view-filter.component.html",
  controller: ViewFilterController
});
